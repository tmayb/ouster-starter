const path = require("path");

module.exports = ({ config }) => {
  // Transpile Gatsby module because Gatsby includes un-transpiled ES6 code.
  config.module.rules[0].exclude = [/node_modules\/(?!(gatsby)\/)/]

  // use installed babel-loader which is v8.0-beta (which is meant to work with @babel/core@7)
  config.module.rules[0].use[0].loader = require.resolve("babel-loader")

  // use @babel/preset-react for JSX and env (instead of staged presets)
  config.module.rules[0].use[0].options.presets = [
    require.resolve("@babel/preset-react"),
    require.resolve("@babel/preset-env"),
  ]

  config.module.rules[0].use[0].options.plugins = [
    // use @babel/plugin-proposal-class-properties for class arrow functions
    require.resolve("@babel/plugin-proposal-class-properties"),
    // use babel-plugin-remove-graphql-queries to remove static queries from components when rendering in storybook
    require.resolve("babel-plugin-remove-graphql-queries"),
  ]

  // Prefer Gatsby ES6 entrypoint (module) over commonjs (main) entrypoint
  config.resolve.mainFields = ["browser", "module", "main"]

  // Find Storybook's default CSS processing rule
  const cssLoaderIndex = config.module.rules.findIndex(
    rule => rule.test.source === `\\.css$`
  );

  if (!Number.isInteger(cssLoaderIndex))
    throw new Error("Could not find Storybook's CSS loader");

  // Exclude CSS Modules from Storybook's standard CSS processing
  config.module.rules[cssLoaderIndex].exclude = /\.module\.css$/;

  // Add specific loader rule for CSS Modules
  config.module.rules.push({
    test: /\.module\.css$/,
    use: [
      { loader: `style-loader` },
      {
        loader: "css-loader",
        options: {
          modules: true,
          importLoaders: 1,
          localIdentName: "[path]-[local]-[hash:base64:5]"
        }
      },
      {
        // Use newer versions of postcss tools
        loader: `postcss-loader`,
        options: {
          plugins: loader => [
            require("postcss-modules-extract-imports")({ root: loader.resourcePath }),
          ]
        }
      }
    ],
    include: path.resolve(__dirname, "../src")
  });

  config.module.rules.push({
    test: /\.stories\.jsx?$/,
    loaders: [require.resolve('@storybook/addon-storysource/loader')],
    enforce: 'pre',
  });



  return config
}