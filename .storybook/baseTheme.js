import { create } from '@storybook/theming';

export default create({
  base: 'light',

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: 'black',
  textInverseColor: 'rgba(255,255,255,0.9)',


  brandTitle: 'Ouster components',
  brandUrl: 'https://example.com',
  brandImage: 'https://placehold.it/350x150',
});