import React from 'react'
import { graphql } from 'gatsby'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import Page from '../components/Page'

class RootIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const homePage = get(this, 'props.data.contentfulPage')
    const navBarData = get(this, 'props.data.contentfulNavigationBar')
    const footerData = get(this, 'props.data.contentfulFooter')
    return (
      <Layout
        location={this.props.location}
        navBarData={navBarData}
        footerData={footerData}
      >
        <div style={{ background: '#fff' }}>
          <Helmet title={siteTitle} />
          <Page data={homePage} />
        </div>
      </Layout>
    )
  }
}

export default RootIndex

// Available gatsby contentful image effects
// https://www.gatsbyjs.org/packages/gatsby-image/
export const pageQuery = graphql`
  {
    site {
      siteMetadata {
        title
      }
    }

    contentfulNavigationBar {
      title
      logo {
        fixed {
          src
        }
      }
      links {
        label
        hoverGradient
        inverted
        order
        childLinks {
          url
          label
          subtitle
          type
          order
          image {
            fixed {
              src
            }
          }
        }
      }
    }

    contentfulPage(slug: { eq: "/" }) {
      title
      slug
      sidebar
      blocks {
        title
        subtitle {
          subtitle
        }
        images {
          fluid(maxWidth: 1900, maxHeight: 821, background: "rgb:000000") {
            ...GatsbyContentfulFluid_withWebp
          }
        }
        order
        type
        components {
          title
          subtitle {
            subtitle
          }
          order
          image {
            fixed {
              src
            }
          }
          link
          sentenceList
        }
      }
    }

    contentfulFooter {
      logo {
        fixed {
          src
        }
      }
      logoAttr {
        width
        height
        maxWidth
        maxHeight
      }
      copyrightText
      links {
        url
        invertOnMobile
        label
        order
      }
    }
  }
`
