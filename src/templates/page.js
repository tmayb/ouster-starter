import React from 'react'
import { Link, graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import Page from '../components/Page'

const PageTemplate = ({ data }) => {
  const siteTitle = data.site.siteMetadata.title
  const pageData = data.contentfulPage
  const navBarData = data.contentfulNavigationBar
  const footerData = data.contentfulFooter
  return (
    <Layout navBarData={navBarData} footerData={footerData}>
      <div style={{ background: '#fff' }}>
        <Helmet title={siteTitle} />
        <Page data={pageData} />
      </div>
    </Layout>
  )
}
export default PageTemplate
export const pageQuery = graphql`
  query($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }

    contentfulNavigationBar {
      title
      logo {
        fixed {
          src
        }
      }
      links {
        label
        hoverGradient
        inverted
        order
        childLinks {
          url
          label
          subtitle
          type
          order
          image {
            fixed {
              src
            }
          }
        }
      }
    }

    contentfulPage(slug: { eq: $slug }) {
      title
      slug
      sidebar
      sidebarHeading
      blocks {
        title
        subtitle {
          subtitle
        }
        images {
          fluid(maxWidth: 1900, maxHeight: 821, background: "rgb:000000") {
            ...GatsbyContentfulFluid_withWebp
          }
          fixed {
            src
          }
          description
        }
        order
        type
        components {
          title
          subtitle {
            subtitle
          }
          order
          image {
            description
            fixed {
              src
            }
          }
          link
          sentenceList
        }
      }
    }

    contentfulFooter {
      logo {
        fixed {
          src
        }
      }
      logoAttr {
        width
        height
        maxWidth
        maxHeight
      }
      copyrightText
      links {
        url
        invertOnMobile
        label
        order
      }
    }
  }
`
