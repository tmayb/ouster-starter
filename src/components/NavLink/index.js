import React, { Component } from 'react'
import { Link } from 'gatsby'
import styled, { css } from 'styled-components'
import styles from './index.module.css'
import ChildNav from '../ChildNav'
import { dynamicSort } from '../../utils'
import { GradientBorder } from '../commons/'
import DownCaret from '../../../assets/images/downcaret.svg'

/* styled component fpr creating Nav links */
const Caret = styled.i`
  border: solid white;
  margin-left: 4px;
  border-width: 0 1px 1px 0;
  display: inline-block;
  padding: 2px;
  top: -2px;

  ${props =>
    props.up &&
    css`
      transform: rotate(-135deg);
    `};

  ${props =>
    props.down &&
    css`
      transform: rotate(45deg);
    `};

  @media (max-width: 450px) {
    border-width: 0 2px 2px 0;
    padding: 3px;
    top: -4px;
    position: relative;
    border-color: #fc9e21;
  }
`

const Li = styled.li`
  display: inline-flex;
  align-items: center;
  margin: 0 1rem;
  cursor: pointer;
  position: relative;
  background-color: ${props => (props.inverted ? '#FFF' : 'transparent')};
  padding: ${props => (props.inverted ? '10px' : '0px')};
  border-radius: 2px;
  text-transform: capitalize;
  font-size: 13px;

  top: -30px;

  height: 90px;

  &:last-child {
    margin-right: 4.3rem;
  }

  &:hover {
    &:before {
      background: ${props => props.hoverGradient};
      left: -2px;
      width: 100%;
      display: block;
      position: absolute;
      top: 0px;
      content: '';
      height: 3px;
      left: ${props => (props.inverted ? '2px' : '0px')};
    }
  }
  a {
    color: ${props => (props.inverted ? '#35459C' : '#FFF')};
  }

  @media (max-width: 450px) {
    display: block;
    font-size: 1.25rem;
    line-height: 1.625rem;
    top: 0px;
    height: auto;
    width: 100%;
    margin: 0px;
    margin-bottom: 1.875rem;
    &:last-child {
      margin-bottom: 0rem;
    }

    &:hover {
      &:before {
        background: none;
      }
    }
  }
`

export default class NavLink extends Component {
  state = {
    isMobile: typeof window !== `undefined` && window.innerWidth <= 450,
    // to track hover over Li
    isHovered: false,
    // to track hover state of child nav
    mouseOverChildSection: false,
    showChildNavs: false,
  }

  handleClick = event => {
    this.setState({ showChildNavs: !this.state.showChildNavs })
  }

  handleMouseOverParent = event => {
    this.setState({ isHovered: true })
  }

  handleMouseOutParent = event => {
    this.setState({ isHovered: false })
  }

  handleMouseOverChild = event => {
    this.setState({ mouseOverChildSection: true })
  }

  handleMouseLeaveChild = event => {
    this.setState({ mouseOverChildSection: false })
  }

  render() {
    const { label, childLinks, inverted, hoverGradient } = this.props
    const { isHovered, mouseOverChildSection } = this.state
    const childProps = { label, childLinks, hoverGradient }
    return (
      <Li
        {...childProps}
        onMouseOver={
          childLinks.length > 1 && !this.state.isMobile
            ? this.handleMouseOverParent
            : null
        }
        onMouseOut={
          childLinks.length > 1 && !this.state.isMobile
            ? this.handleMouseOutParent
            : null
        }
        onClick={
          childLinks.length > 1 && this.state.isMobile ? this.handleClick : null
        }
      >
        {this.state.isMobile && !inverted && (
          <span className={styles.mobileCaretHolder}>
            {childLinks.length > 1 && (
              <Caret
                up={this.state.showChildNavs ? true : false}
                down={!this.state.showChildNavs ? true : false}
              />
            )}
          </span>
        )}

        <Link
          to={childLinks.length === 1 ? childLinks[0].url : null}
          className={styles.link}
        >
          <span className={inverted ? styles.invertedLink : ''}>{label}</span>
        </Link>
        {!this.state.isMobile && childLinks.length > 1 && (
          <Caret
            up={isHovered ? true : false}
            down={!isHovered ? true : false}
          />
        )}

        {(isHovered || mouseOverChildSection || this.state.showChildNavs) &&
          childLinks.length > 1 && (
            <div
              className={styles.childLinks}
              onMouseEnter={
                !this.state.isMobile ? this.handleMouseOverChild : null
              }
              onMouseLeave={
                !this.state.isMobile ? this.handleMouseLeaveChild : null
              }
            >
              <div className={styles.childLinkContainer}>
                {/* render child navs */}
                {childLinks.sort(dynamicSort('order')).map(childLink => (
                  <ChildNav {...childLink} />
                ))}
              </div>
              {!this.state.isMobile && <GradientBorder top="138px" />}
            </div>
          )}
      </Li>
    )
  }
}
