import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'
import { GradientBorder } from '../commons'

const OsSpecifications = props => {
  console.log(props)
  const attrs = props.subtitle.subtitle
    .trim()
    .split('\n')
    .map(attr => {
      let [label, value] = attr.split(';')
      value = value.split('$$')
      return { label: label.trim(), value }
    })

  const comparisonAttrs = props.components[0].subtitle.subtitle
    .trim()
    .split('\n')
  const variants = []
  const comparisons = {}
  const additionalFeatures = []
  let variantHeaders = ['\t']

  props.components.forEach(comp => {
    if (comp.link === 'comparison') {
      variantHeaders.push(comp.title)
      variants.push(comp.title)
      comparisons[comp.title] = comp.subtitle.subtitle.split('\n')
    } else if (comp.link == 'osComparisonHeaders') {
      return
    } else {
      additionalFeatures.push(comp)
    }
  })

  const specRows = []
  const maxLength = Math.round(attrs.length / 2)

  for (let i = 0; i < maxLength; i++) {
    specRows.push(
      <div className={styles.set}>
        <div className={styles.spec}>
          <span className={styles.label}>{attrs[i].label}</span>
          <span className={styles.value}>
            {attrs[i].value.map(val => (
              <span className={styles.subValue}>{val.trim()}</span>
            ))}
          </span>
        </div>
        {i + maxLength <= attrs.length - 1 && (
          <div className={styles.spec}>
            <span className={styles.label}>{attrs[i + maxLength].label}</span>
            <span className={styles.value}>
              {attrs[i + maxLength].value.map(val => (
                <span className={styles.subValue}>{val.trim()}</span>
              ))}
            </span>
          </div>
        )}
      </div>
    )
  }

  return (
    <div className={styles.container}>
      <GradientBorder height="1px" style={{ marginBottom: '19px' }} />
      <h2 className={styles.title}>{props.title}</h2>
      <div className={styles.specs}>{specRows}</div>
      {Object.keys(comparisons).length > 0 && (
        <div className={styles.comparisons}>
          <div className={styles.comparisonHeaders}>
            {variantHeaders.map(header => (
              <h3>{header}</h3>
            ))}
          </div>
          {comparisonAttrs.map((attr, idx) => (
            <div className={styles.comparisonRow}>
              <span className={styles.comparisonAttr}>{attr}</span>
              {variants.map(variant => (
                <div className={styles.variantValueRow}>
                  <span className={styles.mobileVariantName}>{variant}</span>
                  <span>{comparisons[variant][idx]}</span>
                </div>
              ))}
            </div>
          ))}
        </div>
      )}

      <div className={styles.measurements}>
        <Img
          className={styles.measurementImage}
          fluid={props.images[0].fluid}
        />
        <Img
          className={styles.measurementImage}
          fluid={props.images[1].fluid}
        />
      </div>

      <div className={styles.additionalFeatures}>
        <h3 className={styles.additionalFeaturesHeading}>
          {additionalFeatures[0].title}
        </h3>
        <ul className={styles.featurelist}>
          {additionalFeatures[0].subtitle.subtitle
            .trim()
            .split('\n')
            .map(feature => (
              <li>{feature}</li>
            ))}
        </ul>
      </div>
    </div>
  )
}

export default OsSpecifications
