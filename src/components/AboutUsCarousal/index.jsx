import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { ReversedGradientBorder } from '../commons'
import { Link } from 'gatsby'
import Button from '../Button/'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const AboutUsCarousal = ({ title, subtitle, images }) => {
  const sliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 2000,
  }
  const isMobile = typeof window !== `undefined` && window.innerWidth <= 450

  return (
    <div className={styles.container}>
      <ReversedGradientBorder
        className={styles.gradBorder}
        height="3px"
      ></ReversedGradientBorder>
      <div className={styles.textBox}>
        <h2 className={styles.title}>{title}</h2>
        <div className={styles.subtitle}>{subtitle.subtitle}</div>
        {isMobile && (
          <div className={styles.carousal}>
            <Slider {...sliderSettings}>
              {images.map(image => (
                <div className={styles.carousalImageContainer}>
                  <Img
                    fluid={image.fluid}
                    style={{ height: '100%', width: '100%' }}
                  />
                </div>
              ))}
            </Slider>
          </div>
        )}
        <Link to={'/careers'}>
          <Button className={styles.joinUsButton} inverted>
            Join us
          </Button>
        </Link>
      </div>
      {!isMobile && (
        <div className={styles.carousal}>
          <Slider {...sliderSettings}>
            {images.map(image => (
              <div className={styles.carousalImageContainer}>
                <Img
                  fluid={image.fluid}
                  style={{ height: '100%', width: '100%' }}
                />
              </div>
            ))}
          </Slider>
        </div>
      )}
    </div>
  )
}

export default AboutUsCarousal
