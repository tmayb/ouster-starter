import React from 'react'
import styles from './index.module.css'
import { dynamicSort } from '../../utils'
import styled, { css } from 'styled-components'
import Img from 'gatsby-image'
import { GradientBorder } from '../commons'

const LidarArchitecture = props => {
  return (
    <div className={styles.container}>
      <section className={styles.section}>
        <h1 className={styles.title}>{props.title}</h1>
        <div className={styles.specList}>
          {props.components.sort(dynamicSort('order')).map(block => (
            <div className={styles.specBox}>
              {
                <GradientBorder
                  height="1px"
                  style={{
                    marginBottom:
                      typeof window !== `undefined` && window.innerWidth > 450
                        ? '15px'
                        : '14px',
                  }}
                />
              }
              <h2 className={styles.specTitle}>{block.title}</h2>
              <h3 className={styles.specDescription}>
                {block.subtitle.subtitle}
              </h3>
            </div>
          ))}
        </div>
      </section>
      <section className={`${styles.section} ${styles.backdropImageSection}`}>
        <Img
          fluid={props.images[0].fluid}
          style={{ height: '100%', width: '100%' }}
        />
      </section>
    </div>
  )
}

export default LidarArchitecture
