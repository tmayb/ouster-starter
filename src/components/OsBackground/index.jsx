import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'

const OsBackground = props => {
  return (
    <div className={styles.container}>
      <Img className={styles.heroImage} fluid={props.images[0].fluid} />
    </div>
  )
}

export default OsBackground
