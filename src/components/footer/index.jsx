import React, { Component } from 'react'
import { Link } from 'gatsby'
import styled, { css } from 'styled-components'
import styles from './index.module.css'
import { dynamicSort } from '../../utils'
import FooterLogo from '../../../assets/images/Ouster_Symbol_TM_White_RGB.svg'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const FooterContainer = styled.div`
  background-color: #000;
  height: 188px;
  max-width: 100vw;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 60px;
  font-family: 'Spezia';
  color: #fff;

  @media (max-width: 450px) {
    padding: 0 1.625rem;
    padding-bottom: 1.875rem;
    display: block;
    height: auto;
    width: 100%;
  }
`

export default class NavLink extends Component {
  render() {
    const { logo, logoAttr, copyrightText, links } = this.props.data
    const talkToSales = links[0]
    const mobileLinks = links.slice(1)

    return (
      <div>
        <GradientBorder height="5px" />
        <FooterContainer>
          {typeof window !== `undefined` && window.innerWidth > 450 && (
            <React.Fragment>
              <div>
                <div className={`${styles.section} ${styles.logoSection}`}>
                  <FooterLogo className={styles.logo} />
                  <div className={styles.copyrightText}>{copyrightText}</div>
                </div>
                <div className={`${styles.section} ${styles.linkContainer}`}>
                  <div className={styles.linkSection}>
                    {links.sort(dynamicSort('order')).map(link => (
                      <div className={styles.link}>
                        <Link to={link.url}>{link.label}</Link>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className={styles.termsAndPrivacy}>
                <div>Made in San Fransisco</div>
                <div>
                  <Link to="/privacy">
                    <span>Privacy policy</span>
                  </Link>
                </div>
                <div>
                  <Link to="/terms">
                    <span>Terms of site use</span>
                  </Link>
                </div>
              </div>
            </React.Fragment>
          )}

          {typeof window !== `undefined` && window.innerWidth <= 450 && (
            <React.Fragment>
              <div className={`${styles.section} ${styles.linkContainer}`}>
                <div className={styles.mobileLinkSection}>
                  {/* links go here */}
                  {mobileLinks
                    .slice(0, 4)
                    .sort(dynamicSort('order'))
                    .map(link => (
                      <div className={styles.link}>
                        <Link to={link.url}>{link.label}</Link>
                      </div>
                    ))}
                </div>
                <div className={styles.mobileLinkSection}>
                  {/* links go here */}
                  {mobileLinks
                    .slice(4)
                    .sort(dynamicSort('order'))
                    .map(link => (
                      <div className={styles.link}>
                        <Link to={link.url}>{link.label}</Link>
                      </div>
                    ))}
                </div>
              </div>
              <Link to={talkToSales.url}>
                <Button
                  className={styles.talkToSales}
                  style={{
                    width:
                      typeof window !== `undefined` && window.innerWidth > 450
                        ? 'auto'
                        : '100%',
                    margin: '0 auto',
                  }}
                >
                  Talk to Sales
                </Button>
              </Link>
              <div className={`${styles.logoSection}`}>
                <FooterLogo className={styles.logo} />
                <div className={styles.copyrightText}>{copyrightText}</div>
              </div>

              <div className={styles.termsAndPrivacy}>
                <div>Made in San Fransisco</div>
                <div>
                  <Link to="/privacy">
                    <span>Privacy policy</span>
                  </Link>
                </div>
                <div>
                  <Link to="/terms">
                    <span>Terms of site use</span>
                  </Link>
                </div>
              </div>
            </React.Fragment>
          )}
        </FooterContainer>
      </div>
    )
  }
}
