import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const CareerIntro = ({ title, subtitle, images }) => {
  return (
    <div className={styles.container}>
      <div className={styles.descriptionBox}>
        <div className={styles.vCenter}>
          {typeof window !== `undefined` && window.innerWidth <= 450 && (
            <h1 className={styles.title}>{title}</h1>
          )}
          {typeof window !== `undefined` && window.innerWidth > 450 && (
            <h2 className={styles.title}>{title}</h2>
          )}
          <div className={styles.subtitle}>{subtitle.subtitle}</div>
        </div>
      </div>
      <div className={styles.imageBox}>
        <Img
          style={{ height: '100%', width: '100%' }}
          fluid={images[0].fluid}
        />
      </div>
    </div>
  )
}

export default CareerIntro
