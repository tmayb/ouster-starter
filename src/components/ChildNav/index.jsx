import React, { Component } from 'react'
import { Link } from 'gatsby'

import styles from './index.module.css'
import Arrow from '../../../static/arrow.png'

export default class NavLink extends Component {
  render() {
    const { url, label, subtitle, type, image } = this.props

    return (
      <Link to={url}>
        <div className={styles.container}>
          <div className={styles.imageBox}>
            <img
              src={image.fixed.src}
              style={{ width: '100%', height: '100%' }}
            />
          </div>

          <div className={styles.descriptionBlock}>
            <div>
              {typeof window !== `undefined` && window.innerWidth > 450 && (
                <img className={styles.arrow} src={Arrow} />
              )}
              <div className={styles.label}>{label}</div>
            </div>
            <div className={styles.subtitle}>{subtitle}</div>
          </div>
        </div>
      </Link>
    )
  }
}
