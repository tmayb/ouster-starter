import styled, { css } from 'styled-components'

// Let's use styled components
const Button = styled.button`
  background: #35459c;
  color: #fff;
  border-radius: 3px;
  border: 2px solid #35459c;
  padding: 0.25rem 1rem;
  min-width: 230px;
  width: 250px;
  height: 55px;
  font-size: 18px;
  line-height: 130%;
  cursor: pointer;

  :disabled {
    opacity: 0.4;
    cursor: not-allowed;
  }

  ${/* customization for inverted style */ ''}
  ${props =>
    props.inverted &&
    css`
      background: transparent;
      color: #35459c;
    `};

  ${/* customization for size style */ ''}
  ${props =>
    props.small
      ? css`
          min-width: 191px;
        `
      : props.large
      ? css`
          min-width: 304px;
        `
      : css``}

    @media (max-width: 450px) {
    width: 100%;
  }
`

export default Button
