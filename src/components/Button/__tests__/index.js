import 'jest-styled-components';
import React from 'react';
import Button from '../index';
import renderer from 'react-test-renderer';
import { render } from 'react-testing-library'
import {shallow} from 'enzyme'


describe('Regular Button Block', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Button/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('has min width of 250px ', ()=> {
    const tree = renderer.create(<Button />).toJSON();
    expect(tree).toHaveStyleRule('min-width', '250px');
  });

  it('has a background color #35459C', ()=> {
    const tree = renderer.create(<Button />).toJSON();
    expect(tree).toHaveStyleRule('background', '#35459C');
  })

  it('has a text color #FFF', ()=> {
    const tree = renderer.create(<Button />).toJSON();
    expect(tree).toHaveStyleRule('color', '#FFF');
  })
});

describe('Large Button Block', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Button large/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('has min width of 304px ', ()=> {
    const tree = renderer.create(<Button large/>).toJSON();
    expect(tree).toHaveStyleRule('min-width', '304px');
  })
});

describe('Small Button Block', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Button small/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('has min width of 191px ', ()=> {
    const tree = renderer.create(<Button small/>).toJSON();
    expect(tree).toHaveStyleRule('min-width', '191px');
  })
});

describe('Inverted Button Block', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Button inverted/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('has a transparent background', ()=> {
    const tree = renderer.create(<Button inverted/>).toJSON();
    expect(tree).toHaveStyleRule('background', 'transparent');
  })

  it('has a text color #35459C', ()=> {
    const tree = renderer.create(<Button inverted/>).toJSON();
    expect(tree).toHaveStyleRule('color', '#35459C');
  })
});