import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { addParameters } from '@storybook/react';

import Button from './index';


const stories = storiesOf('Button', module);

// Add the `withKnobs` decorator to add knobs support to your stories.
// You can also configure `withKnobs` as a global decorator.
// stories.addDecorator(withKnobs);

stories.add('Default', () => <Button onClick={action('clicked')}>Normal</Button>);
stories.add('Inverted', () => <Button inverted onClick={action('clicked')}>Inverted</Button>)
stories.add('Small', () => <Button small onClick={action('clicked')}>Small</Button>)
stories.add('Small and inverted', () => <Button small inverted onClick={action('clicked')}>Small & inverted</Button>, {
  notes: 'Multiple modifiers can be added together',
})
stories.add('Large', () => <Button large onClick={action('clicked')}>Large Button</Button>)
stories.add('Large and inverted', () => <Button large inverted onClick={action('clicked')}>Large & inverted </Button>)

// // Knobs as dynamic variables.
// stories.add('Dynamic playground', () => {
//   const buttomText = text('Name', 'Button text');


//   const label = 'Size';
//   const options = {
//     Normal: '',
//     Large: 'large',
//     Small: 'small',
//   };
//   const defaultValue = '';

//   const size = select(label, options, defaultValue,);
//   return (<Button {...{ [size]: true }} >{buttomText}</Button>);
// });