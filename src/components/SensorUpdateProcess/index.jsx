import React from 'react'

import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Img from 'gatsby-image'

export default ({ title, components, subtitle, images }) => {
  const frags = subtitle.subtitle.split('\n')
  return (
    <div className={styles.container}>
      <GradientBorder height="1px" className={styles.gradBorder} />
      <h2>{title}</h2>
      <div className={styles.subtext}>{frags[0]}</div>
      <ol className={styles.steps}>
        {frags.slice(1).map(step => (
          <li
            className={styles.step}
            dangerouslySetInnerHTML={{ __html: step }}
          />
        ))}
      </ol>
      <div className={styles.frontImageContainer}>
        <Img className={styles.frontImage} fluid={images[0].fluid} />
      </div>
    </div>
  )
}
