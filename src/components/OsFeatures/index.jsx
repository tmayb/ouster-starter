import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const OsFeatures = props => {
  console.log(props)
  const attrs = props.subtitle.subtitle.split('\n')
  const tts = attrs[0].split(':')[1].trim() === 'true'
  return (
    <div className={styles.container}>
      <GradientBorder height="1px" />
      <h2 className={styles.title}>{props.title}</h2>
      <div className={styles.features}>
        {props.components.map(comp => (
          <div className={styles.feature}>
            <img src={comp.image.fixed.src} className={styles.featureImage} />
            <h3 className={styles.featureTitle}>{comp.title}</h3>
            <div className={styles.featureSubtitle}>
              {comp.subtitle.subtitle}
            </div>
          </div>
        ))}
      </div>
      {tts && (
        <div className={styles.talkToSales}>
          <Button className={styles.tts}>Talk to sales</Button>
        </div>
      )}
    </div>
  )
}

export default OsFeatures
