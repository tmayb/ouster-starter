import React from 'react'
import Img from 'gatsby-image'

import styles from './hero.module.css'

export default ({ title, subtitle, images }) => {
  // assume the strategy is random if not fixed
  let image = images[Math.round(Math.random() * (images.length - 1))]

  return (
    <div className={styles.hero}>
      <Img className={styles.heroImage} fluid={image.fluid} />
      <div className={styles.heroDetails}>
        <h3 className={styles.heroTitle}>{title}</h3>
        <p className={styles.heroSubtitle}>{subtitle.subtitle}</p>
      </div>
    </div>
  )
}
