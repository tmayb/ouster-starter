import React from 'react'

import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

export default ({ title, components }) => {
  return (
    <div className={styles.container}>
      <GradientBorder height="1px" className={styles.gradBorder} />
      <h2>{title}</h2>
      <div className={styles.resources}>
        {components.map(component => (
          <div className={styles.block}>
            <div className={styles.textSection}>
              <h3 className={styles.text}>{component.title}</h3>
              <div className={styles.subtext}>
                {component.subtitle.subtitle}
              </div>
            </div>
            <div className={styles.buttonBox}>
              <Button className={styles.downloadButton} inverted>
                Download
              </Button>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
