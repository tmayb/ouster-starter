import React from 'react'
import styles from './index.module.css'
import YouTube from 'react-youtube'

const CareerVideo = ({ subtitle }) => {
  return (
    <div className={styles.container}>
      <YouTube videoId={subtitle.subtitle} className={styles.videoPreview} />
    </div>
  )
}

export default CareerVideo
