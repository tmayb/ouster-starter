import React from 'react'

const DefaultPageBlock = () => {
  return (
    <div
      style={{
        height: '100vh',
        width: '100%',
        color: '#FFF',
        backgroundColor: '#000',
        fontSize: '5rem',
        paddingTop: '30%',
      }}
    >
      Default component
    </div>
  )
}

export default DefaultPageBlock
