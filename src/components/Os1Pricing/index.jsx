import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const Os1Pricing = props => {
  return (
    <div className={styles.container}>
      <GradientBorder height="1px" className={styles.gradBorder} />
      {typeof window !== `undefined` && window.innerWidth > 450 && (
        <div>
          <div className={styles.variantNames}>
            {props.subtitle.subtitle
              .trim()
              .split('\n')
              .map(price => (
                <div className={styles.variantContainer}>
                  <h2 className={styles.variantName}>{price.split(':')[0]}</h2>
                  <div className={styles.variantPrice}>
                    {price.split(';')[1]}
                  </div>
                </div>
              ))}
          </div>
          <div className={styles.frontImageContainer}>
            <Img className={styles.frontImage} fluid={props.images[0].fluid} />
          </div>

          <div className={styles.ttsContainer}>
            <Button className={styles.tts}>Talk to sales</Button>
            <Button className={styles.dl} inverted>
              Download datasheet PDF
            </Button>
            <div className={styles.volPricingText}>
              Volume pricing available
            </div>
          </div>
        </div>
      )}
      {typeof window !== `undefined` && window.innerWidth <= 450 && (
        <div>
          <div className={styles.frontImageContainer}>
            <Img className={styles.frontImage} fluid={props.images[0].fluid} />
          </div>

          <div className={styles.variantNames}>
            {props.subtitle.subtitle
              .trim()
              .split('\n')
              .map(price => (
                <div className={styles.variantContainer}>
                  <h2 className={styles.variantName}>{price.split(':')[0]}</h2>
                  <div className={styles.variantPrice}>
                    {price.split(';')[1]}
                  </div>
                </div>
              ))}
          </div>
          <div className={styles.ttsContainer}>
            <Button className={styles.tts}>Talk to sales</Button>
            <Button className={styles.dl} inverted>
              Download datasheet PDF
            </Button>
            <div className={styles.volPricingText}>
              Volume pricing available
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default Os1Pricing
