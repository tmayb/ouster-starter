import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const CareerPros = ({ components }) => {
  return (
    <div className={styles.container}>
      {components.map(comp => (
        <div className={styles.section}>
          <GradientBorder height="1px" className={styles.grad} />
          <h2 className={styles.title}>{comp.title}</h2>
          <div className={styles.subtitle}>
            {comp.subtitle.subtitle.split('\n').map(desc => (
              <div className={styles.desc}>{desc}</div>
            ))}
          </div>
        </div>
      ))}
      <div className={styles.buttonbox}>
        <Button>See open positions</Button>
      </div>
    </div>
  )
}

export default CareerPros
