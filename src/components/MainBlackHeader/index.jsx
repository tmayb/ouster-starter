import React from 'react'

import styles from './index.module.css'

export default ({ title }) => {
  return (
    <div className={styles.container}>
      <h2>{title}</h2>
    </div>
  )
}
