import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const CareerIntro = ({ title, subtitle, components }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.title}>{title}</h2>
      <div className={styles.profiles}>
        {components.map(profile => (
          <div className={styles.profile}>
            <img
              src={profile.image.fixed.src}
              className={styles.headshot}
              alt=""
            />
            <h4 className={styles.designation}>{profile.subtitle.subtitle}</h4>
            <div className={styles.name}>{profile.title}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default CareerIntro
