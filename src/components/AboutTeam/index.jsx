import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { ReversedGradientBorder } from '../commons'
import Button from '../Button/'
import { Link } from 'gatsby'

const AboutTeam = ({ title, subtitle, components, images }) => {
  return (
    <div className={styles.container}>
      <ReversedGradientBorder
        className={styles.gradBorder}
        height="3px"
      ></ReversedGradientBorder>
      <div className={`${styles.section} ${styles.titleSection}`}>
        <h2>{title}</h2>
      </div>
      <div className={`${styles.section} ${styles.aboutSection}`}>
        <div className={styles.description}>
          {subtitle.subtitle.split('\n').map(text => (
            <div>{text}</div>
          ))}
        </div>
        <Link to={'/careers'}>
          <Button className={styles.careersButton} inverted>
            {' '}
            See open positions
          </Button>
        </Link>
        <div className={styles.valueSection}>
          <h3 className={styles.valueSectionTitle}>{components[0].title}</h3>
          <div className={styles.values}>
            {components[0].subtitle.subtitle.split('\n').map(value => (
              <div className={styles.valueBox}>
                <h4 className={styles.valueHeading}>{value.split(':')[0]}</h4>
                <div className={styles.value}>{value.split(':')[1]}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className={styles.imageBox}>
        <Img
          style={{ height: '100%', width: '100%' }}
          fluid={images[0].fluid}
        />
      </div>
    </div>
  )
}

export default AboutTeam
