import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'
import Button from '../Button/'
import { Link } from 'gatsby'

export default ({ meta, index }) => {
  const [available, ...specs] = meta.sentenceList.reverse()
  return (
    <React.Fragment>
      {index % 2 != 0 && <div className={styles.divider} />}
      <div className={styles.container}>
        <div className={styles.imageContainer}>
          <Img
            fixed={meta.image.fixed}
            style={{ width: '100%', height: '100%' }}
          />
        </div>
        <div className={styles.title}>{meta.title}</div>
        <div className={styles.shortDescription}>{meta.subtitle.subtitle}</div>
        <section className={styles.specificationsContainer}>
          <ul className={styles.specifications}>
            {specs.reverse().map(spec => (
              <li className={styles.specification}>
                <span>{spec}</span>
              </li>
            ))}
          </ul>
          <span className={styles.discountTag}>Volume discounts available</span>
          <div className={styles.buttonContainer}>
            <Link to={meta.link}>
              <Button className={styles.button}>Learn more</Button>
            </Link>
          </div>
        </section>
      </div>
    </React.Fragment>
  )
}
