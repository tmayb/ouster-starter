import React from 'react'
import YouTube from 'react-youtube'
import styles from './index.module.css'

const OusterInAction = props => {
  const [video1, video2] = props.subtitle.subtitle.split(',')
  return (
    <div className={styles.container}>
      <div className={styles.section}>
        <h2 className={styles.title}>{props.title}</h2>
        <ul className={styles.list}>
          {props.components[0].sentenceList.map(sentence => (
            <li className={styles.listItem}>{sentence}</li>
          ))}
        </ul>
      </div>

      <div className={styles.section}>
        <YouTube videoId={video1} className={styles.videoPreview} />
        <div className={styles.videoDescription}>
          {props.components[1].subtitle.subtitle}
        </div>
      </div>

      <div className={styles.section}>
        <YouTube videoId={video2} className={styles.videoPreview} />
        <div className={styles.videoDescription}>
          {props.components[2].subtitle.subtitle}
        </div>
      </div>
    </div>
  )
}

export default OusterInAction
