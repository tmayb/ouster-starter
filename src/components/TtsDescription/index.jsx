import React from 'react'

import styles from './index.module.css'
import Indicator from '../../../static/arrow-blue.png'

export default ({ title, subtitle }) => {
  const subtitlePoints = subtitle.subtitle.trim().split('\n')
  return (
    <div className={styles.container}>
      <div className={styles.section}>
        <h2>{title}</h2>
      </div>
      <div className={styles.section}>
        <h3>{subtitlePoints[0]}</h3>
        <div className={styles.listContainer}>
          {subtitlePoints.slice(1).map(point => (
            <div>
              <span className={styles.arrowIcon}>
                <img src={Indicator} className={styles.indicator} />
              </span>
              <div className={styles.bulletText}>{point}</div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
