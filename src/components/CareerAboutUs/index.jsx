import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const CareerAboutUs = ({ title, subtitle, components }) => {
  return (
    <div className={styles.container}>
      <div className={styles.section}>
        <h1>{title}</h1>
      </div>
      <div className={`${styles.section} ${styles.list}`}>
        {components.map(comp => (
          <div>
            <h2 className={styles.compTitle}>{comp.title}</h2>
            <div className={styles.description}>{comp.subtitle.subtitle}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default CareerAboutUs
