import React, { Component } from 'react'

import styles from './index.module.css'
import Indicator from '../../../static/arrow-blue.png'
import StickyBox from 'react-sticky-box'

import Hero from '../hero'
import Default from '../DefaultPageBlock'
import ProductHighlight from '../ProductHighlight'
import OusterInAction from '../OusterInAction'
import LidarArchitecture from '../LidarArchitecture'
import SensorHighlight from '../SensorHighlight'
import WorkWithUs from '../WorkWithUs'
import OsHero from '../OsHero'
import OsFeatures from '../OsFeatures'
import OsGeneric from '../OsGeneric'
import OsBackground from '../OsBackground'
import OsSpecifications from '../OsSpecifications'
import Os1Pricing from '../Os1Pricing'
import Os2Pricing from '../Os2Pricing'
import MainBlackHeader from '../MainBlackHeader'
import TtsDescription from '../TtsDescription'
import TtsForm from '../TtsForm'
import DocumentationResources from '../DocumentationResources'
import SensorUpdateProcess from '../SensorUpdateProcess'
import ChangeLog from '../ChangeLog'
import DistributorHeader from '../DistributorHeader'
import DistributorRegion from '../DistribitorRegion'
import CareerIntro from '../CareerIntro'
import CareerPros from '../CareerPros'
import CareerVideo from '../CareerVideo'
import CareerAboutUs from '../CareerAboutUs'
import CareerInfoGraphic from '../CareerInfoGraphic'
import CareerOpenings from '../CareerOpenings'
import AboutOuster from '../AboutOuster'
import AboutTeam from '../AboutTeam'
import Leadership from '../Leadership'
import Investors from '../Investors'
import AboutUsCarousal from '../AboutUsCarousal'

import { dynamicSort } from '../../utils'

export default class page extends Component {
  state = {
    selectedIndex: 0,
  }
  getComponent = data => {
    switch (data.type) {
      case 'hero':
        return <Hero {...data} />

      case 'productHighlight':
        return <ProductHighlight {...data} />

      case 'ousterInAction':
        return <OusterInAction {...data} />

      case 'lidarArchitecture':
        return <LidarArchitecture {...data} />

      case 'sensorHighlight':
        return <SensorHighlight {...data} />

      case 'workWithUs':
        return <WorkWithUs {...data} />

      case 'osHero':
        return <OsHero {...data} />

      case 'osFeatures':
        return <OsFeatures {...data} />

      case 'osGeneric':
        return <OsGeneric {...data} />

      case 'osBackground':
        return <OsBackground {...data} />

      case 'osSpecifications':
        return <OsSpecifications {...data} />

      case 'os1Pricing':
        return <Os1Pricing {...data} />

      case 'os2Pricing':
        return <Os2Pricing {...data} />

      case 'mainBlackHeader':
        return <MainBlackHeader {...data} />

      case 'ttsDescription':
        return <TtsDescription {...data} />

      case 'ttsForm':
        return <TtsForm {...data} />

      case 'documentationResources':
        return <DocumentationResources {...data} />

      case 'sensorUpdateProc':
        return <SensorUpdateProcess {...data} />

      case 'changeLog':
        return <ChangeLog {...data} />

      case 'distributorHeading':
        return <DistributorHeader {...data} />

      case 'distributorRegion':
        return <DistributorRegion {...data} />
      case 'careerIntro':
        return <CareerIntro {...data} />

      case 'careerPros':
        return <CareerPros {...data} />

      case 'careerVideo':
        return <CareerVideo {...data} />

      case 'careerAboutUs':
        return <CareerAboutUs {...data} />

      case 'careerInfoGraphic':
        return <CareerInfoGraphic {...data} />

      case 'careerOpenings':
        return <CareerOpenings {...data} />

      case 'aboutOuster':
        return <AboutOuster {...data} />

      case 'aboutTeam':
        return <AboutTeam {...data} />

      case 'leadership':
        return <Leadership {...data} />

      case 'aboutInvestors':
        return <Investors {...data} />

      case 'aboutUsCarousal':
        return <AboutUsCarousal {...data} />

      default:
        return <Default {...data} />
    }
  }
  render() {
    const blocks = this.props.data.blocks.sort(dynamicSort('order'))
    console.log(blocks)

    return (
      <div className={styles.container}>
        <div className="wrapper">
          {!this.props.data.sidebar && (
            <React.Fragment>
              {this.getComponent(blocks[0])}
              {blocks.slice(1).map(block => this.getComponent(block))}
            </React.Fragment>
          )}
          {this.props.data.sidebar && (
            <div className={styles.container}>
              {typeof window !== `undefined` && window.innerWidth <= 450 && (
                <div className={styles.mobileTopBar}>
                  {this.props.data.blocks.slice(1).map((block, idx) => (
                    <span>
                      <img
                        src={Indicator}
                        alt=""
                        className={`${styles.indicator} ${
                          this.state.selectedIndex != idx ? styles.hidden : ''
                        }`}
                      />
                      <span> {block.title}</span>
                    </span>
                  ))}
                </div>
              )}
              {this.getComponent(this.props.data.blocks[0])}
              <div className={styles.withSidebar}>
                <div className={styles.mainContent}>
                  {this.props.data.blocks
                    .slice(1)
                    .sort(dynamicSort('order'))
                    .map(block => this.getComponent(block))}
                </div>
                {typeof window !== `undefined` && window.innerWidth > 450 && (
                  <StickyBox style={{ width: '25%' }}>
                    <div className={styles.sidebar}>
                      <div className={styles.sidebarWrapper}>
                        <div className={styles.sidebarDesc}>
                          {this.props.data.sidebarHeading
                            ? this.props.data.sidebarHeading
                            : 'On this page'}
                        </div>
                        <div className={styles.links}>
                          {this.props.data.blocks.slice(1).map((block, idx) => (
                            <div>
                              <img
                                alt=""
                                src={Indicator}
                                className={`${styles.indicator} ${
                                  this.state.selectedIndex != idx
                                    ? styles.hidden
                                    : ''
                                }`}
                              />
                              <span> {block.title}</span>
                            </div>
                          ))}
                        </div>
                      </div>
                    </div>
                  </StickyBox>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    )
  }
}
