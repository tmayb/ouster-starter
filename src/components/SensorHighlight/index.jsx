import React, { Component } from 'react'
import styles from './index.module.css'
import Indicator from '../../../static/arrow-blue.png'
import Img from 'gatsby-image'

export default class WorkWithUs extends Component {
  constructor(props) {
    super(props)

    this.imageContainer = React.createRef()
    this.imagePaneHeight = 579
    this.state = {
      currentImageIndex: 0,
      openedBox: 0,
    }
  }

  scrollHandler = event => {
    // change current image index using scroll positions
    let newImageIndex = Math.round(
      (this.imageContainer.current.scrollTop - 20) / this.imagePaneHeight
    )
    if (this.state.currentImageIndex !== newImageIndex) {
      this.setState({ currentImageIndex: newImageIndex })
    }
  }

  openThisBox = idx => {
    this.setState({ openedBox: idx })
  }

  closeThisBox = () => {
    this.setState({ openedBox: -1 })
  }
  render() {
    const { components } = this.props
    const labels = components.map(c => {
      return {
        title: c.title,
        subtitle: c.subtitle.subtitle,
      }
    })
    const images = components.map(c => c.image)
    return (
      <div className={styles.container}>
        <div className={styles.titleBox}>
          <h2 className={styles.title}>{this.props.title}</h2>
          <div className={styles.subtitle}>{this.props.subtitle.subtitle}</div>
        </div>
        <div className={styles.info}>
          <div className={`${styles.section} ${styles.imageLabels}`}>
            {labels.map((label, idx) => (
              <div className={styles.accordionBox}>
                <div className={styles.accordionTitle}>
                  <h3 style={{ display: 'inline-block' }}>{label.title}</h3>
                  {this.state.openedBox !== idx && (
                    <span
                      className={styles.expand}
                      onClick={() => this.openThisBox(idx)}
                    >
                      +
                    </span>
                  )}
                  {this.state.openedBox === idx && (
                    <span
                      className={styles.shrink}
                      onClick={() => this.closeThisBox()}
                    >
                      -
                    </span>
                  )}
                </div>

                <div
                  className={`${styles.description} ${
                    this.state.openedBox === idx
                      ? styles.expandedBox
                      : styles.closedBox
                  }`}
                >
                  {label.subtitle}
                  {idx === this.state.openedBox && (
                    <div className={styles.mobileImagePreview}>
                      <Img
                        fixed={images[idx].fixed}
                        style={{
                          width: '100%',
                          height: '100%',
                        }}
                      />
                    </div>
                  )}
                </div>
              </div>
            ))}
          </div>
          <div
            className={`${styles.section} ${styles.imagePane}`}
            ref={this.imageContainer}
          >
            {this.state.openedBox > -1 && this.state.openedBox < images.length && (
              <Img
                fixed={images[this.state.openedBox].fixed}
                style={{
                  width: '100%',
                  height: '100%',
                }}
              />
            )}
          </div>
        </div>
      </div>
    )
  }
}
