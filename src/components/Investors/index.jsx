import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { ReversedGradientBorder } from '../commons'
import Button from '../Button/'

const Investors = ({ title, subtitle, images }) => {
  const isMobile = typeof window !== `undefined` && window.innerWidth <= 450
  return (
    <div className={styles.container}>
      <ReversedGradientBorder
        className={styles.gradBorder}
        height="3px"
      ></ReversedGradientBorder>
      <h1 className={styles.title}>{title}</h1>
      <div className={styles.subtitle}>{subtitle.subtitle}</div>
      <div className={styles.investorLogos}>
        {images.map(image => (
          <div
            className={styles.imageBox}
            style={{
              height: `${
                !isMobile
                  ? image.description.split('x')[1]
                  : parseInt(image.description.split('x')[1]) / 2
              }px`,
            }}
          >
            <img
              src={image.fixed.src}
              className={styles.investorLogo}
              style={{
                width: `${
                  !isMobile
                    ? image.description.split('x')[0]
                    : parseInt(image.description.split('x')[0]) / 2
                }px`,
                height: `${
                  !isMobile
                    ? image.description.split('x')[1]
                    : parseInt(image.description.split('x')[1]) / 2
                }px`,
              }}
              alt=""
            />
          </div>
        ))}
      </div>
    </div>
  )
}

export default Investors
