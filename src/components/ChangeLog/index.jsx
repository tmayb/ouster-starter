import React, { useState } from 'react'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Arrow from '../../../static/arrow-down.png'

export default ({ title, components }) => {
  console.log(components)
  const [copiedIndex, setCopiedIndex] = useState(-1)
  return (
    <div className={styles.container}>
      <GradientBorder height="1px" className={styles.gradBorder} />
      <h2>{title}</h2>
      <div className={styles.changeLogs}>
        {components.map((log, idx) => (
          <div className={styles.log}>
            <div className={styles.logHeader}>
              <div className={styles.leftSide}>
                <h3>{log.title}</h3>
                <span className={styles.logDate}>
                  {log.subtitle.subtitle.split('\n')[0]}
                </span>
              </div>
              <div className={styles.rightSide}>
                <a href={log.subtitle.subtitle.split('\n')[1]}>
                  <span>{`Download v${log.title}`}</span>{' '}
                  <img className={styles.downArrow} src={Arrow} />
                </a>
              </div>
            </div>
            <div className={styles.updates}>
              <h4 className={styles.specialH4}>UPDATES</h4>
              <ul className={styles.updateList}>
                {log.subtitle.subtitle
                  .trim()
                  .split('\n')
                  .slice(3)
                  .map(update => (
                    <li>{update}</li>
                  ))}
              </ul>
            </div>
            <div className={styles.sha}>
              <h4 className={styles.specialH4}>SHA256 HASH</h4>
              <div className={styles.hashContainer}>
                <span className={styles.hash}>
                  {log.subtitle.subtitle.split('\n')[2]}
                </span>
                <span className={styles.copySha}>
                  <CopyToClipboard
                    text={log.subtitle.subtitle.split('\n')[2]}
                    onCopy={() => setCopiedIndex(idx)}
                  >
                    {copiedIndex === idx ? (
                      <span style={{ color: 'green' }}>Copied.</span>
                    ) : (
                      <span>Copy</span>
                    )}
                  </CopyToClipboard>
                </span>
              </div>
            </div>
            {log.sentenceList && log.sentenceList.length > 0 && (
              <div className={styles.extraNote}>
                <h4 className={styles.specialH4}>NOTE</h4>
                <span>{log.sentenceList[0]}</span>
              </div>
            )}
          </div>
        ))}
      </div>
    </div>
  )
}
