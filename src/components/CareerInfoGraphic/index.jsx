import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const CareerInfoGraphic = ({ title, images, subtitle, components }) => {
  console.log(components)
  const imageByText = components[0].subtitle.subtitle.split('\n')
  return (
    <div className={styles.container}>
      <div className={styles.imageBox}>
        <Img
          style={{ height: '100%', width: '100%' }}
          fluid={images[0].fluid}
        />
        <div className={styles.byText}>
          <div className={styles.description}>{imageByText[0]}</div>
          <div className={styles.name}>{imageByText[1]}</div>
          <div className={styles.designation}>{imageByText[2]}</div>
        </div>
      </div>
      <div className={styles.textBox}>{subtitle.subtitle}</div>
    </div>
  )
}

export default CareerInfoGraphic
