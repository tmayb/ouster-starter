import React from 'react'

import styles from './index.module.css'

export default ({ title, components }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.title}>{title}</h2>
      {components.map((distributor, idx) => (
        <div className={styles.wrapper}>
          <div className={styles.distributorContainer}>
            <div className={styles.logoContainer}>
              <img src={distributor.image.fixed.src} className={styles.logo} />
            </div>
            <div className={styles.info}>
              <h3 className={styles.name}>{distributor.title}</h3>
              {typeof window !== `undefined` && window.innerWidth > 450 && (
                <div className={styles.attributes}>
                  <div className={styles.col}>
                    <h4>Regions Served</h4>
                    <h4>Services</h4>
                  </div>
                  <div className={styles.col}>
                    <div>
                      {distributor.subtitle.subtitle.trim().split('\n')[0]}
                    </div>
                    <div>
                      {distributor.subtitle.subtitle.trim().split('\n')[1]}
                    </div>
                  </div>
                </div>
              )}

              {typeof window !== `undefined` && window.innerWidth <= 450 && (
                <div className={styles.attributes}>
                  <div className={styles.row}>
                    <h4>Regions Served</h4>
                    <div>
                      {distributor.subtitle.subtitle.trim().split('\n')[0]}
                    </div>
                  </div>
                  <div className={styles.col}>
                    <h4>Services</h4>
                    <div>
                      {distributor.subtitle.subtitle.trim().split('\n')[1]}
                    </div>
                  </div>
                </div>
              )}

              {typeof window !== `undefined` && window.innerWidth > 450 && (
                <div className={styles.description}>
                  {distributor.subtitle.subtitle.trim().split('\n')[2]}
                </div>
              )}
            </div>
          </div>
          {typeof window !== `undefined` && window.innerWidth <= 450 && (
            <div className={styles.description}>
              {distributor.subtitle.subtitle.trim().split('\n')[2]}
            </div>
          )}
        </div>
      ))}
    </div>
  )
}
