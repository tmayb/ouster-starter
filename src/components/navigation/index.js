import React, { Component } from 'react'
import { Link } from 'gatsby'
import styles from './index.module.css'
import styled, { css } from 'styled-components'
import NavLink from '../NavLink'
import { dynamicSort } from '../../utils'
import Logo from '../../../assets/images/Ouster_Logo_TM_Horiz_White_RGB.svg'
import HamBurger from '../../../assets/images/HamBurger.svg'
import CloseButton from '../../../assets/images/close.svg'
import OusterText from '../../../assets/images/OusterText.svg'
import { GradientBorder } from '../commons'
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks,
} from 'body-scroll-lock'

export default class Navigation extends Component {
  state = {
    isMobile: typeof window !== `undefined` && window.innerWidth <= 450,
    transparentNav: true,
    smart: false,
    prevScrollpos: 200,
    visible: true,
    toggleHeight: 0,
    childNavData: null,
    sideDrawerVisible: false,
  }

  targetRef = React.createRef()
  targetElement = null

  componentDidMount() {
    this.targetElement = this.targetRef.current
    const { smart, smartToggleHeight } = this.props
    const toggleHeight = smartToggleHeight ? smartToggleHeight : 45
    if (smart) {
      this.setState({ smart: true, toggleHeight })
      window.addEventListener('scroll', this.handleScroll)
    }
  }

  componentWillUnmount = () => {
    clearAllBodyScrollLocks()

    window.removeEventListener('scroll', this.handleScroll)
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.smart && this.props.smart) {
      let toggleHeight = 0
      if (prevProps.smartToggleHeight != this.props.smartToggleHeight) {
        toggleHeight = this.props.smartToggleHeight
          ? this.props.smartToggleHeight
          : window.innerHeight
      }
      this.setState({ smart: true, toggleHeight })
      window.addEventListener('scroll', this.handleScroll)
    } else {
      if (!prevProps.smart && !this.props.smart) {
        this.setState({ smart: false })
        window.removeEventListener('scroll', this.handleScroll)
      }
    }
  }

  setChildNavData = data => {
    if (data === null) {
      if (this.state.mouseOverChildSection === true) {
        this.setState({ clearNavData: true }, () => {})
      } else {
      }
    } else {
      this.setState({
        childNavData: data,
        mouseOverChildSection: true,
        clearNavData: false,
      })
    }
  }

  handleScroll = () => {
    const { prevScrollpos, toggleHeight } = this.state

    const currentScrollPos = window.pageYOffset
    const visible =
      currentScrollPos < toggleHeight || prevScrollpos > currentScrollPos

    this.setState({
      prevScrollpos: currentScrollPos,
      visible,
    })
  }

  handleHamburgerClick = event => {
    this.setState({ sideDrawerVisible: true })
    disableBodyScroll(this.targetElement)
  }

  handleCrossClick = event => {
    this.setState({ sideDrawerVisible: false })
    enableBodyScroll(this.targetElement)
  }

  render() {
    const { visible, prevScrollpos, toggleHeight } = this.state
    const { links, solid, fixed, smart, bgColor, smartSolid } = this.props
    let navFillClass =
      solid || (smartSolid && smart) ? `${styles.solidNav}` : ''
    let fixedClass = smart || fixed ? `${styles.fixedNav}` : styles.relative
    let customStyles = {}
    if (bgColor) {
      customStyles['backgroundColor'] = bgColor
    }
    return (
      <div className={styles.container}>
        {false && this.state.isMobile && (
          <GradientBorder
            height="5px"
            mbot="5px"
            className={styles.gradBorder}
          />
        )}
        <nav
          role="navigation"
          className={`${
            styles.navigationContainer
          } clearfix  ${navFillClass} ${fixedClass} ${
            !visible && smart ? styles.hiddenNavigation : ''
          } `}
          style={customStyles}
        >
          <ul className={styles.navigation}>
            <li className={styles.logo}>
              <Link to="/">
                <Logo style={{ width: '100%', height: '100%' }} />
              </Link>
            </li>

            {links
              .sort(dynamicSort('order'))
              .map(({ label, childLinks, inverted, hoverGradient }) => {
                const navLinkProps = {
                  label,
                  childLinks,
                  inverted,
                  hoverGradient,
                }
                return (
                  <NavLink
                    {...navLinkProps}
                    updateChildData={this.setChildNavData}
                  />
                )
              })}
          </ul>

          <div className={styles.mobileNavigation} ref={this.targetRef}>
            <div className={styles.mobileHeader}>
              <div>
                <OusterText className={styles.logoText} />
              </div>
              <div>
                {!this.state.sideDrawerVisible && (
                  <HamBurger
                    className={styles.hamburger}
                    onClick={this.handleHamburgerClick}
                  />
                )}
              </div>
              {this.state.sideDrawerVisible && (
                <div>
                  <CloseButton
                    className={styles.closeButton}
                    onClick={this.handleCrossClick}
                  />
                </div>
              )}
            </div>
            <div
              className={`${styles.sideDrawer} ${
                !this.state.sideDrawerVisible ? styles.hidden : ''
              }`}
            >
              <ul className={styles.mobileNavList}>
                {links
                  .sort(dynamicSort('order'))
                  .map(({ label, childLinks, inverted, hoverGradient }) => {
                    const navLinkProps = {
                      label,
                      childLinks,
                      inverted,
                      hoverGradient,
                    }
                    return (
                      <NavLink
                        {...navLinkProps}
                        updateChildData={this.setChildNavData}
                      />
                    )
                  })}
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}
