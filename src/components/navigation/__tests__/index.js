import 'jest-styled-components'
import React from 'react'
import Navigation from '../index'
import renderer from 'react-test-renderer'
import logo from '../../../../assets/images/logo.png'

const mockData = {
  logo,
  links: [
    {
      label: 'entry1',
      url: '/1',
      hoverGradient: '',
      inverted: false,
      childLinks: [
        {
          url: '/documentation',
          label: 'documentation',
          subtitle: null,
          type: 'plain',
          image: null,
        },
      ],
    },
    {
      label: 'entry2',
      url: '/2',
      inverted: false,
      childLinks: [
        {
          url: '/products/os1',
          label: 'OS1',
          subtitle:
            'Mid-range lidar sensor in 16 and 64 channel configurations',
          type: 'rich',
          image: {
            fixed: {
              src:
                '//images.ctfassets.net/tjhl2s1515g8/3EXzqpRF1eMCQy6L63Ry27/53c0565482d6213325cce1f2bb25666b/os-1__1_.png?w=400&q=50',
            },
          },
        },
        {
          url: '/products/os2',
          label: 'OS2',
          subtitle:
            'Long-range lidar sensor for high-speed autonomous navigation',
          type: 'rich',
          image: {
            fixed: {
              src:
                '//images.ctfassets.net/tjhl2s1515g8/2ZBaZtpDjoQQIC17Iounn3/5702fc7cee7b0304f8262cd2787d2a2a/os-2__1_.png?w=400&q=50',
            },
          },
        },
      ],
    },
    {
      label: 'entry3',
      url: '/3',
      inverted: false,
      childLinks: [
        {
          url: '/distributors',
          label: 'distributors',
          subtitle: null,
          type: 'plain',
          image: null,
        },
      ],
    },
    {
      label: 'entry4',
      url: '/4',
      inverted: true,
      inverted: false,
      childLinks: [
        {
          url: '/blog',
          label: 'blog',
          subtitle: null,
          type: 'plain',
          image: null,
        },
      ],
    },
  ],
}

// Snapshot testing
describe('Regular Navblock', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<Navigation {...mockData} />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
