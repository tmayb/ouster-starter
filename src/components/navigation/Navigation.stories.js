import React from 'react'

import { storiesOf } from '@storybook/react'
// import { action } from '@storybook/addon-actions'
import { addParameters } from '@storybook/react'
import logo from '../../../assets/images/logo.png'

import Navigation from './index'

const stories = storiesOf('Navigation', module)

const mockData = {
  logo,
  links: [
    {
      label: 'entry1',
      url: '/1',
      hoverGradient: '',
      inverted: false,
      childLinks: [
        {
          url: '/documentation',
          label: 'documentation',
          subtitle: null,
          type: 'plain',
          image: null,
        },
      ],
    },
    {
      label: 'entry2',
      url: '/2',
      inverted: false,
      childLinks: [
        {
          url: '/products/os1',
          label: 'OS1',
          subtitle:
            'Mid-range lidar sensor in 16 and 64 channel configurations',
          type: 'rich',
          image: {
            fixed: {
              src:
                '//images.ctfassets.net/tjhl2s1515g8/3EXzqpRF1eMCQy6L63Ry27/53c0565482d6213325cce1f2bb25666b/os-1__1_.png?w=400&q=50',
            },
          },
        },
        {
          url: '/products/os2',
          label: 'OS2',
          subtitle:
            'Long-range lidar sensor for high-speed autonomous navigation',
          type: 'rich',
          image: {
            fixed: {
              src:
                '//images.ctfassets.net/tjhl2s1515g8/2ZBaZtpDjoQQIC17Iounn3/5702fc7cee7b0304f8262cd2787d2a2a/os-2__1_.png?w=400&q=50',
            },
          },
        },
      ],
    },
    {
      label: 'entry3',
      url: '/3',
      inverted: false,
      childLinks: [
        {
          url: '/distributors',
          label: 'distributors',
          subtitle: null,
          type: 'plain',
          image: null,
        },
      ],
    },
    {
      label: 'entry4',
      url: '/4',
      inverted: true,
      inverted: false,
      childLinks: [
        {
          url: '/blog',
          label: 'blog',
          subtitle: null,
          type: 'plain',
          image: null,
        },
      ],
    },
  ],
}

stories.add('Default(solid)', () => (
  <div>
    <Navigation {...mockData} />
    <div
      style={{ height: '1200px', width: '100%', backgroundColor: 'orange' }}
    />
    <div
      style={{ height: '1200px', width: '100%', backgroundColor: 'green' }}
    />
  </div>
))

stories.add('Solid', () => (
  <div>
    <Navigation {...mockData} solid />
    <div style={{ height: '800px', width: '100%', backgroundColor: 'red' }} />
    <div
      style={{ height: '800px', width: '100%', backgroundColor: 'yellow' }}
    />
  </div>
))

stories.add('Solid with custom background', () => (
  <div>
    <Navigation {...mockData} solid bgColor="green" />
    <div style={{ height: '800px', width: '100%', backgroundColor: 'red' }} />
    <div
      style={{ height: '800px', width: '100%', backgroundColor: 'yellow' }}
    />
  </div>
))

stories.add('Solid & fixed', () => (
  <div>
    <Navigation {...mockData} solid fixed />
    <div style={{ height: '800px', width: '100%', backgroundColor: 'green' }} />
    <div
      style={{ height: '800px', width: '100%', backgroundColor: 'yellow' }}
    />
  </div>
))

stories.add('Fixed', () => (
  <div style={{ backgroundColor: '#017DEB' }}>
    <Navigation {...mockData} fixed />
    <div style={{ height: '800px', width: '100%', backgroundColor: 'pink' }} />
    <div
      style={{ height: '800px', width: '100%', backgroundColor: 'purple' }}
    />
  </div>
))

stories.add('Smart', () => (
  <div style={{ backgroundColor: '#017DEB' }}>
    <Navigation {...mockData} smart />
    <div style={{ height: '100vh', width: '100%', backgroundColor: 'red' }} />
    <div
      style={{ height: '200vh', width: '100%', backgroundColor: 'purple' }}
    />
  </div>
))

stories.add('Smart & solid', () => (
  <div style={{ backgroundColor: '#017DEB' }}>
    <Navigation {...mockData} smart smartSolid />
    <div style={{ height: '100vh', width: '100%', backgroundColor: 'green' }} />
    <div
      style={{ height: '200vh', width: '100%', backgroundColor: 'orange' }}
    />
  </div>
))
