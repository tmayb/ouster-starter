import styled, { css } from 'styled-components'

// implement gradient border using div
export const GradientBorder = styled.div`
  width: ${props => (props.width ? props.width : '100%')};
  height: ${props => (props.height ? props.height : '5px')};
  background: linear-gradient(
    to right,
    #150461,
    #c724b0,
    #d50032,
    #ff3d45,
    #fc9e21
  );
  position: relative;
  top: ${props => (props.top ? props.top : '0px')};
`

export const ReversedGradientBorder = styled.div`
  width: ${props => (props.width ? props.width : '100%')};
  height: ${props => (props.height ? props.height : '5px')};
  background: linear-gradient(
    to left,
    #150461,
    #c724b0,
    #d50032,
    #ff3d45,
    #fc9e21
  );
  position: relative;
  top: ${props => (props.top ? props.top : '0px')};
`
