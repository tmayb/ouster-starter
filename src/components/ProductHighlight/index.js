import React from 'react'
import { Link } from 'gatsby'
import styles from './index.module.css'
import Product from '../ProductSnippet'
import { dynamicSort } from '../../utils'

export default ({ title, subtitle, components }) => {
  return (
    <div className={styles.container}>
      <div className={styles.title}>{title}</div>

      <div className={styles.subtitle}>{subtitle.subtitle}</div>
      <section className={styles.backdrop} />

      <section className={styles.products}>
        {components.sort(dynamicSort('order')).map((block, index) => (
          <Product meta={block} index={index} />
        ))}
      </section>
    </div>
  )
}
