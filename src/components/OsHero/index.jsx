import React from 'react'
import Img from 'gatsby-image'

import styles from './index.module.css'

export default ({ title, subtitle, images }) => {
  // assume the strategy is random if not fixed
  const texts = subtitle.subtitle.split('\n')
  const isOS1 = texts[0] === 'OS1'
  const isMobile = typeof window !== `undefined` && window.innerWidth <= 450

  return (
    <div
      className={`${styles.container} ${
        !isOS1 ? styles.invertedColorScheme : ''
      }`}
    >
      <div>
        <Img
          className={styles.heroImage}
          fluid={!isMobile ? images[0].fluid : images[1].fluid}
        />
      </div>

      <div className={styles.details}>
        <span className={styles.label}>LIDAR SENSOR</span>
        <h1>{texts[0]}</h1>
        <div className={styles.osTitle}>
          {title.split(',').map(frag => (
            <h3 className={styles.titleFrag}>{frag}</h3>
          ))}
        </div>
      </div>
      <div
        className={
          texts[0] === 'OS1'
            ? `${styles.body} ${styles.os1Body}`
            : `${styles.body} ${styles.os2Body}`
        }
      >
        {texts[1]}
      </div>
    </div>
  )
}
