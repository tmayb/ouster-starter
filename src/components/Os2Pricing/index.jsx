import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const Os1Pricing = props => {
  return (
    <div className={styles.container}>
      <GradientBorder height="1px" className={styles.gradBorder} />
      <div className={styles.pricingBox}>
        <Img className={styles.frontImage} fluid={props.images[0].fluid} />
        <div className={styles.pricingText}>
          <h1 className={styles.title}>OS2</h1>
          <div className={styles.subtext}>
            {props.subtitle.subtitle.split('\n').map(line => (
              <div>{line}</div>
            ))}
          </div>
          <div className={styles.ttsConainer}>
            <Button className={styles.tts}>Talk to sales</Button>
            <Button className={styles.dl} inverted>
              Download datasheet PDF
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Os1Pricing
