import React from 'react'
import Img from 'gatsby-image'

import styles from './index.module.css'

export default ({ title, images, subtitle }) => {
  const fragments = subtitle.subtitle.split('\n')
  return (
    <div className={styles.container}>
      <div className={styles.textBox}>
        <h1 className={styles.title}>{title}</h1>
        {typeof window !== `undefined` && window.innerWidth <= 450 && (
          <div className={styles.imageBox}>
            <Img
              style={{ height: '100%', width: '100%' }}
              fluid={images[0].fluid}
            />
          </div>
        )}
        <div className={styles.definition}>
          <h2 className="inline-block">{fragments[0]}</h2>
          <span className={`${styles.noun} ${styles.bottomAlign}`}>noun.</span>
          <span className={`${styles.phonetic} ${styles.bottomAlign}`}>
            \ ˈau̇-stər \
          </span>
          <ol className={`steps ${styles.defList}`}>
            {fragments.slice(1).map(item => (
              <li className={styles.defItem}>{item}</li>
            ))}
          </ol>
        </div>
      </div>
      {typeof window !== `undefined` && window.innerWidth > 450 && (
        <div className={styles.imageBox}>
          <Img
            style={{ height: '100%', width: '100%' }}
            fluid={images[0].fluid}
          />
        </div>
      )}
    </div>
  )
}
