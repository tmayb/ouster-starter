import React from 'react'
import styles from './index.module.css'
import Img from 'gatsby-image'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const OsGeneric = props => {
  console.log(props)
  let withImage = props.images && props.images.length > 0 ? true : false

  return (
    <div className={styles.container}>
      {((typeof window !== `undefined` && window.innerWidth <= 450) ||
        !withImage) && (
        <GradientBorder height="1px" style={{ marginBottom: '19px' }} />
      )}
      {!withImage && (
        <div>
          <div
            className={styles.description}
            style={{ width: '85%', margin: '0 auto' }}
          >
            {props.subtitle.subtitle}
          </div>
        </div>
      )}
      {withImage && (
        <div className={styles.flexContainer}>
          <div className={styles.section}>
            {typeof window !== `undefined` && window.innerWidth > 450 && (
              <GradientBorder height="1px" style={{ marginBottom: '19px' }} />
            )}
            <h2 className={styles.title}>{props.title}</h2>
            <div className={styles.description}>{props.subtitle.subtitle}</div>
          </div>
          <div className={styles.section}>
            <Img className={styles.image} fluid={props.images[0].fluid} />
          </div>
        </div>
      )}
    </div>
  )
}

export default OsGeneric
