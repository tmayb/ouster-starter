import React from 'react'

import styles from './index.module.css'

export default ({ title, subtitle }) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>{title}</h1>
      <div className={styles.subtitle}>{subtitle.subtitle}</div>
    </div>
  )
}
