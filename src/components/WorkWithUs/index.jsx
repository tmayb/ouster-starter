import React from 'react'
import Img from 'gatsby-image'
import styles from './index.module.css'
import { GradientBorder } from '../commons'
import Button from '../Button/'

const WorkWithUs = props => {
  return (
    <div className={styles.container}>
      <GradientBorder
        height="1px"
        style={{ marginBottom: '15px' }}
        className={styles.mobileGrad}
      />

      <div className={styles.imageContainer}>
        <Img
          fluid={props.images[0].fluid}
          style={{ height: '100%', width: '100%' }}
        />
      </div>

      <div className={styles.description}>
        <GradientBorder
          height="1px"
          style={{ marginBottom: '29px' }}
          className={styles.normalGrad}
        />

        <h2 className={styles.title}>{props.title}</h2>
        <div className={styles.jobIncentive}>{props.subtitle.subtitle}</div>
        <Button
          inverted
          style={{
            margin: '2.5rem 0 0',
            width:
              typeof window !== `undefined` && window.innerWidth > 450
                ? 'auto'
                : '100%',
          }}
        >
          See open positions
        </Button>
      </div>
    </div>
  )
}

export default WorkWithUs
