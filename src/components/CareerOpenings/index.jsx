import React from 'react'
import styles from './index.module.css'
import AccordionBox from './AccordionBox'

const CareerOpenings = ({ title, components }) => {
  return (
    <div className={styles.container}>
      <h2 className={styles.title}>{title}</h2>
      <div className={styles.accordions}>
        {components.map(comp => (
          <AccordionBox
            title={comp.title}
            blocks={comp.subtitle.subtitle.split('\n')}
          />
        ))}
      </div>
    </div>
  )
}

export default CareerOpenings
