import React, { useState } from 'react'
import styles from './index.module.css'
import Indicator from '../../../static/arrow-blue.png'
import styled, { css } from 'styled-components'

/* styled component fpr creating Nav links */
const Caret = styled.i`
  border: solid #5e79fb;
  margin-left: 0px;
  border-width: 0 2px 2px 0;
  display: inline-block;
  padding: 7px;
  top: 4px;
  cursor: pointer;

  ${props =>
    props.up &&
    css`
      transform: rotate(-135deg);
    `};

  ${props =>
    props.down &&
    css`
      transform: rotate(45deg);
    `};

  @media (max-width: 450px) {
    border-width: 0 2px 2px 0;
    padding: 3px;
    top: -4px;
    position: relative;
  }
`

const AccordionBox = ({ title, blocks }) => {
  console.log(blocks)
  const [opened, setOpened] = useState(false)
  return (
    <div className={styles.accordion}>
      <div className={styles.accordionHeader}>
        <h3 className={styles.accordionTitle}>{title}</h3>
        <Caret
          up={opened ? true : false}
          down={!opened ? true : false}
          onClick={() => setOpened(!opened)}
        />
      </div>
      <div
        className={`${styles.foldable} ${
          opened ? styles.opened : styles.closed
        }`}
      >
        {blocks.map(block => (
          <div className={styles.accordionBlock}>
            <div>{block.split(':')[0]}</div>
            <div>{block.split(':')[1]}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default AccordionBox
